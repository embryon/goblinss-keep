package component;

import com.artemis.Component;

/**
 * Basic button data
 */
public class Button extends Component {
    
    private boolean pressed;
    
    public Button() {
        setPressed(false);
    }
    
    public boolean isPressed() {
        return pressed;
    }
    
    public boolean isReleased() {
        return !isPressed();
    }

    public void setPressed(boolean pressed) {
        this.pressed = pressed;
    }

    public void press() {
        setPressed(true);
    }

    public void release() {
        setPressed(false);
    }
}
