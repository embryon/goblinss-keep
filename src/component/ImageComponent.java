package component;

import com.artemis.Component;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Stores an image for an entity
 */
public class ImageComponent extends Component {
    
    private Image image;
    
    public ImageComponent(String path) throws SlickException {
        setImage(path);
    }
    
    public Image getImage() {
        return image;
    }
    
    public void setImage(String path) throws SlickException {
        image = new Image(path);
    }
}