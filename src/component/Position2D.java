package component;

import com.artemis.Component;

/**
 * Position with in x,y coordinates
 */
public class Position2D extends Component {
    
    protected float x;
    protected float y;

    public Position2D() {
    }

    public Position2D(float x, float y) {
        setPosition(x, y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setPosition(float x, float y) {
        setX(x);
        setY(y);
    }

    public void addX(float x) {
        setX(getX() + x);
    }

    public void addY(float y) {
        setY(getY() + y);
    }
    
}
