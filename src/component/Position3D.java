package component;

/**
 * Component used to give a x,y,z position to an entity
 */
public class Position3D extends Position2D {
    
    private float z;

    public Position3D() {
    }

    public Position3D(float x, float y, float z) {
        setPosition(x, y, z);
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setPosition(float x, float y, float z) {
        super.setPosition(x, y);
        
        setZ(z);
    }

    public void addZ(float z) {
        setZ(getZ() + z);
    }
    
}