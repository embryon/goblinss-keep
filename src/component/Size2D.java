package component;

import com.artemis.Component;

/**
 * Contain width and height
 */
public class Size2D extends Component {
    
    private int width;
    private int height;
    
    public Size2D() {
        setSize(0, 0);
    }
    
    public Size2D(int width, int height) {
        setSize(width, height);
    }
    
    public void setSize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
    
}
