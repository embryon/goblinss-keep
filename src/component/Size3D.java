package component;

/**
 * Size component with length, width and height
 */
public class Size3D extends Size2D{
    
    private int length;
    
    public Size3D() {
        setSize(0, 0, 0);
    }
    
    public Size3D(int width, int height, int length) {
        setSize(width, height, length);
    }
    
    public void setSize(int width, int length, int height) {
        super.setSize(width, height);
        
        setLength(length);
    }
    
    public int getLength() {
        return length;
    }
    
    public void setLength(int length) {
        this.length = length;
    }
    
}
