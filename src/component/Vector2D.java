package component;

/**
 * Simple vector in 2 dimensions
 */
public class Vector2D extends Position2D {
    
    public Vector2D(float x, float y) {
        setVector(x, y);
    }
    
    public Vector2D() {
        this(0, 0);
    }
    
    public void setVector(float x, float y) {
        setX(x);
        setY(y);
    }
    
    public void add(Vector2D vector) {
        add(vector.getX(), vector.getY());
    }
    
    public void add(float x, float y) {
        setX(this.x + x);
        setY(this.y + y);
    }
    
}
