package component;

import com.artemis.Component;

/**
 * Zoom component
 */
public class Zoom extends Component {
    
    private double level;
    private final double step;
    private final double max;
    private final double min;
    
    public Zoom() {
        this(1.00, 0.25, 0.00, 8.00);
    }
    
    public Zoom(double level, double step, double min, double max) {
        this.level = level;
        this.step = step;
        this.min = min;
        this.max = max;
    }
    
    public double getLevel() {
        return level;
    }
    
    public void zoomUp() {
        level = Math.min(level + step, max);
    }
    
    public void zoomDown() {
        level = Math.max(level - step, min);
    }
    
    public void zoomDefault() {
        level = 1.00;
    }
    
}
