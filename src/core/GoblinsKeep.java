package core;

import com.artemis.World;
import com.artemis.managers.TagManager;
import core.state.Init;
import core.state.Game;
import core.state.Menu;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import system.action.Action;
import system.action.ActionMapping;
import system.action.ActionSystem;

/**
 * Contains the program main loop's logic
 */
public class GoblinsKeep extends StateBasedGame {
    
    private final World world;
    private final ActionMapping actionMapping;
        
    public GoblinsKeep() {
        super("Goblins's Keep");
        
        world = new World();
        world.setManager(new TagManager());
        world.setSystem(new ActionSystem());
        
        // Default input mapping
        actionMapping = new ActionMapping();
        actionMapping.setMouseButtonAction(Input.MOUSE_LEFT_BUTTON, Action.MOUSE_LEFT_CLICK);
        actionMapping.setMouseButtonAction(Input.MOUSE_MIDDLE_BUTTON, Action.MOUSE_MIDDLE_CLICK);
        actionMapping.setMouseButtonAction(Input.MOUSE_RIGHT_BUTTON, Action.MOUSE_RIGHT_CLICK);
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        addState(new Init(world, actionMapping));
        addState(new Menu(world, actionMapping));
        addState(new Game(world, actionMapping));
    }
    
    /**
     * Allows states to get the World
     * @return Current World
     */
    public World getWorld() {
        return world;
    }

    /**
     * Get default actions associated with inputs
     * @return The default input/actions mapping
     */
    public ActionMapping getDefaultActionMapping() {
        return actionMapping;
    }
    
}
