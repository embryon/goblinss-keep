package core;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 * Program's entry point
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            AppGameContainer appgc;
            appgc = new AppGameContainer(new GoblinsKeep());
            appgc.setDisplayMode(800, 600, false);
            appgc.setVSync(true);
            appgc.setMaximumLogicUpdateInterval(10);
            appgc.start();
        }
        catch (SlickException ex) {
            Logger.getLogger(GoblinsKeep.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
