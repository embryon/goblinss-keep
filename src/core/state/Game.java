package core.state;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import component.Position3D;
import component.Size2D;
import component.Zoom;
import core.terrain.Terrain;
import core.terrain.Tile;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import system.CameraSystem;
import system.TerrainRendererSystem;
import system.action.Action;
import system.action.ActionMapping;
import system.action.ActionSystem;

/**
 * Game state for the main menu
 */
public class Game extends GameState {

    private TerrainRendererSystem terrainRenderer;
    private CameraSystem cameraSystem;
    private Entity camera;
    private Entity terrain;

    public Game(World world, ActionMapping actionMapping) {
        super(world, actionMapping);
        
        this.actionMapping.setKeyAction(Input.KEY_Z, Action.CAMERA_UP);
        this.actionMapping.setKeyAction(Input.KEY_S, Action.CAMERA_DOWN);
        this.actionMapping.setKeyAction(Input.KEY_Q, Action.CAMERA_LEFT);
        this.actionMapping.setKeyAction(Input.KEY_D, Action.CAMERA_RIGHT);
        this.actionMapping.setKeyAction(Input.KEY_A, Action.CAMERA_FLOOR_UP);
        this.actionMapping.setKeyAction(Input.KEY_E, Action.CAMERA_FLOOR_DOWN);
        this.actionMapping.setKeyAction(Input.KEY_UP, Action.CAMERA_UP);
        this.actionMapping.setKeyAction(Input.KEY_DOWN, Action.CAMERA_DOWN);
        this.actionMapping.setKeyAction(Input.KEY_LEFT, Action.CAMERA_LEFT);
        this.actionMapping.setKeyAction(Input.KEY_RIGHT, Action.CAMERA_RIGHT);
        this.actionMapping.setMouseButtonAction(Input.MOUSE_MIDDLE_BUTTON, Action.CAMERA_ZOOM_DEFAULT);
        this.actionMapping.setMouseWheelUpAction(Action.CAMERA_ZOOM_UP);
        this.actionMapping.setMouseWheelDownAction(Action.CAMERA_ZOOM_DOWN);
    }
    
    @Override
    public int getID() {
        return StateID.GAME.ordinal();
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        // Initialize a new Terrain
        Terrain terrainComponent = new Terrain(100, 100, 50);
        terrainComponent.generate();
        
        terrain = world.createEntity();
        terrain.addComponent(terrainComponent);
        terrain.addToWorld();
        
        world.getManager(TagManager.class).register("TERRAIN", terrain);
        
        // Camera
        camera = world.createEntity();
        camera.addComponent(new Zoom(1.00, 0.10, 0.20, 1.50));
        camera.addComponent(new Position3D(0,0,0));
        camera.addComponent(new Size2D(container.getWidth(),container.getHeight()));
        camera.addToWorld();
        
        world.getManager(TagManager.class).register("CAMERA", camera);
        
        // Systems
        terrainRenderer = new TerrainRendererSystem();
        cameraSystem = new CameraSystem(container.getGraphics());
        
        cameraSystem.init(world);
        
    }
    
    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        ActionSystem actionSystem = world.getSystem(ActionSystem.class);
        actionSystem.setInputActionMap(actionMapping);
        
        world.setSystem(terrainRenderer);
        world.setSystem(cameraSystem);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        world.setDelta(delta);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        world.process();
    }
    
    @Override
    public void leave(GameContainer container, StateBasedGame game)  {
        world.deleteSystem(terrainRenderer);
        world.deleteSystem(cameraSystem);
    }
    
}
