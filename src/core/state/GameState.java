package core.state;

import com.artemis.World;
import org.newdawn.slick.state.BasicGameState;
import system.action.ActionMapping;

/**
 * Extended BasicGameState with additionnal specific logic
 */
public abstract class GameState extends BasicGameState {
    
    protected World world;
    protected ActionMapping actionMapping;
    
    public GameState(World world, ActionMapping actionMapping) {
        this.world = world;
        this.actionMapping = new ActionMapping(actionMapping);
    }
    
}
