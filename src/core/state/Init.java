package core.state;

import com.artemis.World;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import system.ButtonSystem;
import system.action.ActionSystem;
import system.RendererSystem;
import system.action.ActionMapping;

/**
 * Initial state of the program, only used to load common World data
 */
public class Init extends GameState {

    public Init(World world, ActionMapping actionMapping) {
        super(world, actionMapping);
    }

    @Override
    public int getID() {
        return StateID.INIT.ordinal();
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        ActionSystem actionSystem = world.getSystem(ActionSystem.class);
        
        // Initialize systems
        ButtonSystem buttonSystem = new ButtonSystem();
        RendererSystem rendererSystem = new RendererSystem();
        
        // Adds systems to the world
        world.setSystem(buttonSystem);
        world.setSystem(rendererSystem);
        
        // Link the input system to the Slick2D input system
        container.getInput().addMouseListener(actionSystem);
        container.getInput().addKeyListener(actionSystem);
        
        buttonSystem.register();
        
        world.initialize();
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        game.enterState(StateID.MENU.ordinal());
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
    }
    
}
