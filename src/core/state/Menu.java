package core.state;

import com.artemis.Entity;
import com.artemis.World;
import component.Button;
import component.ImageComponent;
import component.Position2D;
import component.Size2D;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import system.action.ActionMapping;
import system.action.ActionSystem;

/**
 * Game state for the main menu
 */
public class Menu extends GameState {

    private Entity button;

    public Menu(World world, ActionMapping actionMapping) {
        super(world, actionMapping);
    }
    
    @Override
    public int getID() {
        return StateID.MENU.ordinal();
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        button = world.createEntity();
        button.addComponent(new Button());
        button.addComponent(new Size2D(292,292));
        button.addComponent(new Position2D(100,100));
        button.addComponent(new ImageComponent("res/img/test.png"));
    }
    
    @Override
    public void enter(GameContainer container, StateBasedGame game) {
        ActionSystem actionSystem = world.getSystem(ActionSystem.class);
        actionSystem.setInputActionMap(actionMapping);
                
        button.addToWorld();
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        world.setDelta(delta);
        
        if(button.getComponent(Button.class).isPressed()) {
            game.enterState(StateID.GAME.ordinal(), new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
        }
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        world.process();
    }
    
    @Override
    public void leave(GameContainer container, StateBasedGame game)  {
        button.deleteFromWorld();
    }
    
}
