package core.state;

/**
 * Store all state's ID so there is no duplicate
 */
public enum StateID {
    
    INIT,
    MENU,
    GAME,
    
}
