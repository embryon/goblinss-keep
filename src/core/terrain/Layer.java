package core.terrain;

import component.Size2D;

/**
 * Stores a layer of tiles
 */
public class Layer {
    
    private final Size2D size;
    private final Tile[][] l_tiles;
    
    public Layer(int width, int height) {
        size = new Size2D(width, height);
        
        // Initialize the tile list
        l_tiles = new Tile[width][height];
    }
    
    public int getWidth() {
        return size.getWidth();
    }
    
    public int getHeight() {
        return size.getHeight();
    }
    
    public Tile getTile(int x, int y) {
        return l_tiles[x][y];
    }
    
    public void setTile(int x, int y, Tile tile) {
        l_tiles[x][y] = tile;
    }
}
