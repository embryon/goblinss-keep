package core.terrain;

import com.artemis.Component;
import component.Size3D;
import java.util.ArrayList;
import java.util.List;
import util.SimplexNoise;

/**
 * Store an ordered list of Layer to represent a terrain with a given depth
 * corresponding to the layers count
 */
public class Terrain extends Component {
    
    private final Size3D size;
    private final List<Layer> l_layers;
    
    public Terrain(int width, int length, int height) {
        size = new Size3D(width, length, height);
        
        // Initialize the layer list
        l_layers = new ArrayList<Layer>();
        for(int i=0; i<height; i++) {
            l_layers.add(new Layer(width, length));
        }
    }
    
    /**
     * Generate a random terrain based on heightmaps
     */
    public void generate() {
        SimplexNoise simplexNoise = new SimplexNoise(100,0.1,5000);
        
        for(int x=0; x<getWidth(); x++) {
            for(int y=0; y<getLength(); y++) {
                double noise = 0.5 * (1 + simplexNoise.getNoise(x, y));
                int height = (int) (noise * getHeight());
                
                for(int z=0; z<height; z++) {
                    getLayer(z).setTile(x, y, Tile.ROCK);
                }
                
                getLayer((int) height).setTile(x, y, Tile.GRASS);
            }
        }
    }
    
    public int getWidth() {
        return size.getWidth();
    }
    
    public int getLength() {
        return size.getLength();
    }
    
    public int getHeight() {
        return size.getHeight();
    }
    
    public Layer getLayer(int height) {
        return l_layers.get(height);
    }
    
    public Tile getTile(int x, int y, int z) {
        return getLayer(z).getTile(x, y);
    }
    
}
