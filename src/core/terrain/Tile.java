package core.terrain;

/**
 * List of existing tiles
 */
public enum Tile {
    
    EMPTY,
    GRASS,
    ROCK,
    
}
