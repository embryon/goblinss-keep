package system;

import system.action.ActionSystem;
import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import component.Button;
import component.Position2D;
import component.Size2D;
import system.action.Action;
import system.action.ActionContext;
import system.action.ActionListener;

/**
 * Manage buttons
 */
public class ButtonSystem extends EntitySystem implements ActionListener {
    
    private ImmutableBag<Entity> entities;
    
    @Mapper ComponentMapper<Button> buttonMap;
    @Mapper ComponentMapper<Position2D> positionMap;
    @Mapper ComponentMapper<Size2D> sizeMap;

    public ButtonSystem() {
        super(Aspect.getAspectForAll(Button.class, Position2D.class, Size2D.class));
        
        this.process();
    }
    
    public void register() {
        world.getSystem(ActionSystem.class).addActionListener(this, Action.MOUSE_LEFT_CLICK);
    }

    protected void process(Entity entity, int x, int y) {
        Button button = buttonMap.get(entity);
        Position2D position = positionMap.get(entity);
        Size2D size = sizeMap.get(entity);
        
        if(x >= position.getX() && x < position.getX() + size.getWidth() &&
           y >= position.getY() && y < position.getY() + size.getHeight()) {
            button.press();
        } else {
            button.release();
        }
    }

    @Override
    public void onAction(Action action, ActionContext context) {
        // We only want to react when the button is pressed
        if(!context.isMouseButtonPressed) return;
        
        int x = (int)context.mousePosition.getX();
        int y = (int)context.mousePosition.getY();
        
        for(int i=0; i<entities.size(); i++) {
            Entity entity = entities.get(i);
            process(entity, x, y);
        }
    }
    
    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {
        this.entities = entities;
    }

    @Override
    protected boolean checkProcessing() {
        return !isPassive();
    }
    
}