package system;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import component.Position3D;
import component.Vector2D;
import component.Zoom;
import core.terrain.Terrain;
import java.util.EnumMap;
import java.util.Map;
import org.newdawn.slick.Graphics;
import system.action.Action;
import system.action.ActionContext;
import system.action.ActionListener;
import system.action.ActionSystem;

/**
 * Manage the camera movements
 */
public class CameraSystem extends VoidEntitySystem implements ActionListener {
    
    private final Graphics graphics;
    private final double speed = 1;
    private final Map<Action, Vector2D> direction;
    private final Vector2D acceleration;
            
    
    public CameraSystem(Graphics graphics) {
        this.graphics = graphics;
        
        acceleration = new Vector2D(0, 0);
        
        direction = new EnumMap<Action, Vector2D>(Action.class);
        direction.put(Action.CAMERA_LEFT, new Vector2D(-1, 0));
        direction.put(Action.CAMERA_RIGHT, new Vector2D(1, 0));
        direction.put(Action.CAMERA_UP, new Vector2D(0, -1));
        direction.put(Action.CAMERA_DOWN, new Vector2D(0, 1));
    }
    
    public void init(World world) {
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_LEFT);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_RIGHT);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_UP);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_DOWN);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_FLOOR_UP);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_FLOOR_DOWN);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_ZOOM_UP);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_ZOOM_DOWN);
        world.getSystem(ActionSystem.class).addActionListener(this, Action.CAMERA_ZOOM_DEFAULT);
    }

    @Override
    protected void processSystem() {
        Entity _terrain = world.getManager(TagManager.class).getEntity("TERRAIN");
        Terrain terrain = _terrain.getComponent(Terrain.class);
        
        Zoom zoom = getCamera().getComponent(Zoom.class);
        Position3D position = getCamera().getComponent(Position3D.class);
        
        float x = (float) (position.getX() + acceleration.getX() * speed * world.getDelta());
        float y = (float) (position.getY() + acceleration.getY() * speed * world.getDelta());
        
        x = Math.max(0, x);
        y = Math.max(0, y);
        
        x = (float) Math.min(x, TerrainRendererSystem.TILE_WIDTH * zoom.getLevel() * terrain.getWidth());
        y = (float) Math.min(y, TerrainRendererSystem.TILE_HEIGHT * zoom.getLevel() * terrain.getLength());
        
        position.setX(x);
        position.setY(y);
        
        graphics.drawString("Floor: " + position.getZ(), 0, 0);
    }

    @Override
    public void onAction(Action action, ActionContext context) {
        Entity _terrain = world.getManager(TagManager.class).getEntity("TERRAIN");
        Terrain terrain = _terrain.getComponent(Terrain.class);
        
        Zoom zoom = getCamera().getComponent(Zoom.class);
        Position3D position = getCamera().getComponent(Position3D.class);

        switch(action) {
            case CAMERA_ZOOM_UP:
                zoom.zoomUp();
                break;
            case CAMERA_ZOOM_DOWN:
                zoom.zoomDown();
                break;
            case CAMERA_ZOOM_DEFAULT:
                if(context.isMouseButtonPressed) {
                    zoom.zoomDefault();
                }
                break;
            case CAMERA_FLOOR_UP:
                if(context.isKeyPressed) {
                    position.setZ(Math.min(position.getZ() + 1, terrain.getHeight()));
                }
                break;
            case CAMERA_FLOOR_DOWN:
                if(context.isKeyPressed) {
                    position.setZ(Math.max(0, position.getZ() - 1));
                }
                break;
            // MOVE UP/DOWN/LEFT/RIGHT
            default:
                int factor = 1;
                if(!context.isKeyPressed) {
                    factor = -1;
                }

                acceleration.add(
                        direction.get(action).getX() * factor,
                        direction.get(action).getY() * factor
                );
                break;
        }
        
    }
    
    private Entity getCamera() {
        return world.getManager(TagManager.class).getEntity("CAMERA");
    }
    
}
