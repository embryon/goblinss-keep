package system;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import component.ImageComponent;
import component.Position2D;

public class RendererSystem extends EntityProcessingSystem {
    
    @Mapper ComponentMapper<Position2D> positionMap;
    @Mapper ComponentMapper<ImageComponent> imageMap;

    public RendererSystem() {
        super(Aspect.getAspectForAll(Position2D.class, ImageComponent.class));
    }

    @Override
    protected void process(Entity entity) {
        Position2D position = positionMap.get(entity);
        ImageComponent image = imageMap.get(entity);

        // Draw the image
        image.getImage().draw(position.getX(), position.getY());
    }
}
