package system;

import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import component.Position3D;
import component.Size2D;
import component.Zoom;
import core.terrain.Layer;
import core.terrain.Terrain;
import core.terrain.Tile;
import java.util.EnumMap;
import java.util.Map;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Use to render the terrain layer
 */
public class TerrainRendererSystem extends VoidEntitySystem {
    
    public static final double TILE_WIDTH = 16;
    public static final double TILE_HEIGHT = 16;
    private final Map<Tile, Image> tiles;
    
    public TerrainRendererSystem() throws SlickException {
        // Load tiles
        tiles = new EnumMap<Tile, Image>(Tile.class);
        tiles.put(Tile.GRASS, new Image("res/img/grass.png"));
        tiles.put(Tile.ROCK, new Image("res/img/rock.png"));
    }

    @Override
    protected void processSystem() {
        Entity _terrain = world.getManager(TagManager.class).getEntity("TERRAIN");
        Terrain terrain = _terrain.getComponent(Terrain.class);
        
        Entity camera = world.getManager(TagManager.class).getEntity("CAMERA");
        Zoom zoom = camera.getComponent(Zoom.class);
        Position3D position = camera.getComponent(Position3D.class);
        Size2D size = camera.getComponent(Size2D.class);
        
        Layer layer = terrain.getLayer((int) position.getZ());
        
        int tileW = (int) (TILE_WIDTH * zoom.getLevel());
        int tileH = (int) (TILE_HEIGHT * zoom.getLevel());
        
        // Center the camera to the middle of the screen
        int baseOffsetX = (int) (position.getX() - (size.getWidth() / 2));
        int baseOffsetY = (int) (position.getY() - (size.getHeight() / 2));
        
        int startX = (int) Math.max(0, Math.floor(baseOffsetX/tileW));
        int startY = (int) Math.max(0, Math.floor(baseOffsetY/tileH));
        
        // Number of tiles to draw
        int w = (int) Math.min(Math.ceil(size.getWidth()/tileW + 1), layer.getWidth());
        int h = (int) Math.min(Math.ceil(size.getHeight()/tileH + 1), layer.getHeight());
        
        int endX = Math.min(startX + w, layer.getWidth());
        int endY = Math.min(startY + h, layer.getHeight());
        
        for(int x=startX; x<endX; x++) {
            for(int y=startY; y<endY; y++) {
                Image image = tiles.get(layer.getTile(x, y));
                
                if(image == null) continue;
                
                image.draw(
                        (x * tileW) - baseOffsetX, 
                        (y * tileH) - baseOffsetY, 
                        (float) zoom.getLevel()
                );
            }
        }
    }
    
}
