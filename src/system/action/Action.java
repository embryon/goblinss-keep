package system.action;

/**
 * Action that can be mapped to differents input
 */
public enum Action {
    
    NONE,
    
    // Mouse actions
    MOUSE_MOTION,
    MOUSE_LEFT_CLICK,
    MOUSE_MIDDLE_CLICK,
    MOUSE_RIGHT_CLICK,
    MOUSE_WHEEL_UP,
    MOUSE_WHEEL_DOWN,
    
    // Camera manipulation
    CAMERA_LEFT,
    CAMERA_RIGHT,
    CAMERA_UP,
    CAMERA_DOWN,
    CAMERA_FLOOR_UP,
    CAMERA_FLOOR_DOWN,
    CAMERA_ZOOM_UP,
    CAMERA_ZOOM_DOWN,
    CAMERA_ZOOM_DEFAULT,
}
