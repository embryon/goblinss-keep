package system.action;

import component.Position2D;

/**
 * Provides informations about an action
 */
public class ActionContext {
    
    public Position2D oldMousePosition;
    public Position2D mousePosition;
    public int mouseButton;
    public boolean isMouseButtonPressed;
    
    public int key;
    public boolean isKeyPressed;
    
}
