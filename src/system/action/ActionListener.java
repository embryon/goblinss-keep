package system.action;

/**
 * Interface for listener to a specific action
 */
public interface ActionListener {
    
    public void onAction(Action action, ActionContext context);
    
}
