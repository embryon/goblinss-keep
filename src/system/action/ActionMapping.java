package system.action;

import java.util.Map;
import util.DefaultedMap;

/**
 * Used to map input to actions
 */
public class ActionMapping {
    
    private final Map<Integer, Action> keyToAction;
    private final Map<Integer, Action> mouseButtonToAction;
    private Action mouseWheelUpAction;
    private Action mouseWheelDownAction;
    
    public ActionMapping() {
        keyToAction = new D​efaultedMap<Integer, Action>(Action.NONE);
        mouseButtonToAction = new D​efaultedMap<Integer, Action>(Action.NONE);
        mouseWheelUpAction = Action.NONE;
        mouseWheelDownAction = Action.NONE;
    }

    /**
     * Copy constructor
     * @param actionMapping Object to be copied
     */
    public ActionMapping(ActionMapping actionMapping) {
        keyToAction = new D​efaultedMap<Integer, Action>(Action.NONE);
        mouseButtonToAction = new D​efaultedMap<Integer, Action>(Action.NONE);
        
        for(int key: actionMapping.getKeyToAction().keySet()) {
            setKeyAction(key, actionMapping.getKeyAction(key));
        }
        
        for(int key: actionMapping.getMouseButtonToAction().keySet()) {
            setMouseButtonAction(key, actionMapping.getMouseButtonAction(key));
        }
        
        mouseWheelUpAction = actionMapping.getMouseWheelUpAction();
        mouseWheelDownAction = actionMapping.getMouseWheelDownAction();
    }
    
    /**
     * Get the Map that assign key to actions
     * @return A map that assigns keys to actions
     */
    public Map<Integer, Action> getKeyToAction() {
        return keyToAction;
    }
    
    /**
     * Returns the map that assigns a mouse boutton to an action
     * @return A map that assigns a mouse button to an action
     */
    public Map<Integer, Action> getMouseButtonToAction() {
        return mouseButtonToAction;
    }
    
    public Action getKeyAction(int key) {
        return keyToAction.get(key);
    }
    
    public Action getMouseButtonAction(int button) {
        return mouseButtonToAction.get(button);
    }
    
    public Action getMouseWheelUpAction() {
        if(mouseWheelUpAction == null) return Action.NONE;
        return mouseWheelUpAction;
    }
    
    public Action getMouseWheelDownAction() {
        if(mouseWheelDownAction == null) return Action.NONE;
        return mouseWheelDownAction;
    }
    
    public void setKeyAction(int key, Action action) {
        keyToAction.put(key, action);
    }
    
    public void setMouseButtonAction(int button, Action action) {
        mouseButtonToAction.put(button, action);
    }
    
    public void setMouseWheelUpAction(Action action) {
        mouseWheelUpAction = action;
    }
    
    public void setMouseWheelDownAction(Action action) {
        mouseWheelDownAction = action;
    }
    
}
