package system.action;

import com.artemis.systems.VoidEntitySystem;
import component.Position2D;
import org.newdawn.slick.Input;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;

/**
 * Translates inputs into actions and notify listeners of these action with the 
 * corresponding input's data
 */
public class ActionSystem extends VoidEntitySystem implements MouseListener, KeyListener {
    
    private final EnumMap<Action, Set<ActionListener>> actionListeners;
    private ActionMapping actionMapping;
    
    public ActionSystem() {
        actionListeners = new EnumMap<Action, Set<ActionListener>>(Action.class);
        
        for(Action action: Action.values()) {
            actionListeners.put(action, new HashSet<ActionListener>());
        }
        
        actionMapping = new ActionMapping();
    }

    /**
     * Set the input mapping to be used
     * @param actionMapping Action/Inputs mapping to use
     */
    public void setInputActionMap(ActionMapping actionMapping) {
        this.actionMapping = actionMapping;
    }
    
    /**
     * Adds a listener to be notified when given action is taken
     * @param listener Listener to notify
     * @param action Action for which the listner will be notified
     */
    public void addActionListener(ActionListener listener, Action action) {
        // Nobody can hook on the NONE action (undefined behaviour)
        if(action == Action.NONE) return;
        
        actionListeners.get(action).add(listener);
    }
    
    /**
     * Notify listeners for a particular action in a given context
     * @param action Action that has been taken
     * @param context Informations about the input
     */
    private void notifyListener(Action action, ActionContext context) {
        for(ActionListener listener: actionListeners.get(action)) {
            listener.onAction(action, context);
        }
    }
        
    /**
     * Manage inputs received when a button of the mouse's state is changed
     * @param button Concerned mouse button
     * @param x X position of the mouse when the evenement occured
     * @param y Y position of the mouse when the evenement occured
     * @param pressed Says wether the button is pressed or released
     */
    private void mouseButton(int button, int x, int y, boolean pressed) {
        ActionContext context = new ActionContext();
        context.mouseButton = button;
        context.mousePosition = new Position2D(x, y);
        context.isMouseButtonPressed = pressed;
        
        switch(button) {
        case Input.MOUSE_LEFT_BUTTON:
            notifyListener(actionMapping.getMouseButtonAction(button), context);
            break;
        case Input.MOUSE_MIDDLE_BUTTON:
            notifyListener(actionMapping.getMouseButtonAction(button), context);
            break;
        case Input.MOUSE_RIGHT_BUTTON:
            notifyListener(actionMapping.getMouseButtonAction(button), context);
            break;
        }
    }
    
    @Override
    public void mousePressed(int button, int x, int y) {
        mouseButton(button, x, y, true);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        mouseButton(button, x, y, false);
    }

    @Override
    public void mouseMoved(int oldX, int oldY, int x, int y) {
        ActionContext context = new ActionContext();
        context.oldMousePosition = new Position2D(oldX, oldY);
        context.mousePosition = new Position2D(x, y);
        
        notifyListener(Action.MOUSE_MOTION, context);
    }
    
    @Override
    public void keyPressed(int key, char c) {
        ActionContext context = new ActionContext();
        context.key = key;
        context.isKeyPressed = true;
        
        notifyListener(actionMapping.getKeyAction(key), context);
    }

    @Override
    public void keyReleased(int key, char c) {
        ActionContext context = new ActionContext();
        context.key = key;
        context.isKeyPressed = false;
        
        notifyListener(actionMapping.getKeyAction(key), context);
    }
    
    @Override
    public void mouseWheelMoved(int change) {
        if(change < 0) {
            notifyListener(actionMapping.getMouseWheelDownAction(), new ActionContext());
        } else {
            notifyListener(actionMapping.getMouseWheelUpAction(), new ActionContext());
        }
    }
    
    @Override public void processSystem() {}
    @Override public void inputEnded() {}
    @Override public void inputStarted() {}
    @Override public void mouseClicked(int button, int x, int y, int clickCount) {}
    @Override public void mouseDragged(int oldX, int oldY, int x, int y) {}
    @Override public void setInput(Input input) {}
    @Override public boolean isAcceptingInput() { return true; }

    @Override
    protected boolean checkProcessing() {
        return !isPassive();
    }
    
}
