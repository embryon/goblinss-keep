package util;

import java.util.HashMap;

/**
 * Map that returns a default value instead of null when no element are mapped
 * to a given key
 * @param <K> Key type
 * @param <V> Value type
 */
public class D​efaultedMap<K,V> extends HashMap<K,V> {
    protected V defaultValue;
    
    public D​efaultedMap(V defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    @Override
    public V get(Object k) {
        return containsKey((K)k) ? super.get(k) : defaultValue;
    }
    
}
